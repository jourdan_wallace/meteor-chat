Router.route('/', function () {
    this.layout('ApplicationLayout');
    this.render('welcome');
}, {
    name: "home"
});

Router.route('/post/:_id', function () {
    this.layout('ApplicationLayout');
     this.render('post', {
         data: function () {
         return Posts.findOne({_id: this.params._id});
         }
     });
Router.route('/search', function () {
    this.render('search');
});
});